package com.virgin.airport.app.airportapp.service;


import com.virgin.airport.app.airportapp.model.Flight;
import java.io.IOException;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FlightAvailabilityServiceTest {


  @Autowired
  private FlightAvailabilityService flightAvailabilityService;



  @Test
  public void shouldCheckAllFlightDetailsLoadedFromCVS() throws IOException {
    List<Flight> flights = flightAvailabilityService.loadFlights();
    System.out.println("fligts::"+flights);
  }

  @Test
  public void shouldGetFlightDetailsWithGivenDay(){
    List<Flight> sundayServiceFlights = flightAvailabilityService.loadFlights("Sunday");
    Assert.assertNotNull(sundayServiceFlights);
  }

}
