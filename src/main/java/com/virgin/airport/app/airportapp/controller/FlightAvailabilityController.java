package com.virgin.airport.app.airportapp.controller;

import com.virgin.airport.app.airportapp.model.Days;
import com.virgin.airport.app.airportapp.model.Flight;
import com.virgin.airport.app.airportapp.service.FlightAvailabilityService;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/virgin/v1")
public class FlightAvailabilityController {

  /**
   * what is the filed injection / constructon injection & setter injection whihch is the best one?
   */
  FlightAvailabilityService flightAvailabilityService;

  public FlightAvailabilityController(FlightAvailabilityService flightAvailabilityService){
    this.flightAvailabilityService = flightAvailabilityService;
  }

  @GetMapping("flights/{date}")
  public ResponseEntity loadFlightDetails(@PathVariable String date) throws Exception{
    try{
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      Date serviceDate = sdf.parse(date);
      Calendar cal = Calendar.getInstance();
      cal.setTime(serviceDate);
      int servicedateDayVaue =cal.get(Calendar.DAY_OF_WEEK);
      System.out.println("dayVaue::"+servicedateDayVaue);
      Days[] daysArray = Days.values();
      Days requestedDay = Arrays.asList(daysArray).stream().filter(day -> day.getValue() == servicedateDayVaue).findAny().orElse(null);
      List<Flight> flight = flightAvailabilityService.loadFlights(requestedDay.getDay());
      return  new ResponseEntity<>(flight, HttpStatus.ACCEPTED);
    }catch (ResponseStatusException e){
      return ResponseEntity.status(e.getStatus()).build();
    }

  }
}
