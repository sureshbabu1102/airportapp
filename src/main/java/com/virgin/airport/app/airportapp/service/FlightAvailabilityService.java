package com.virgin.airport.app.airportapp.service;

import com.opencsv.CSVReader;
import com.virgin.airport.app.airportapp.model.Days;
import com.virgin.airport.app.airportapp.model.Flight;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class FlightAvailabilityService {

  private static List<Flight> flights;

  public FlightAvailabilityService() throws IOException {
    flights = loadFlights();
  }

  public List<Flight> loadFlights(String day){
    List<Flight> flightsByDay = flights.stream().filter(flight -> flight.getServiceDays().contains(day))
        .collect(Collectors.toList());
    return flightsByDay;
  }


  public List<Flight> loadFlights() throws IOException {
    Reader reader = Files.newBufferedReader(Paths.get("src/flights.csv"));
    CSVReader csvReader = new CSVReader(reader);
    String[] row;
    List<Flight> flights = new ArrayList<>();
    while ((row = csvReader.readNext()) != null) {
      Flight flight = new Flight();
      flight.setDepartureTime(row[0]);
      flight.setDestination(row[1]);
      flight.setDestinationAirportIATA(row[2]);
      flight.setFlightNo(row[3]);
      List<String> serviceDays = Arrays
          .asList(row[4], row[5], row[6], row[7], row[8], row[9], row[10]);
      List<String> operateDays = new ArrayList<>();
      if (!serviceDays.stream().allMatch(day -> day.isEmpty())) {
        if (StringUtils.isNotEmpty(row[4])) {
          operateDays.add(Days.SUNDAY.getDay());
        }
        if (StringUtils.isNotEmpty(row[5])) {
          operateDays.add(Days.MONDAY.getDay());

        }
        if (StringUtils.isNotEmpty(row[6])) {
          operateDays.add(Days.TUESDAY.getDay());
        }
        if (StringUtils.isNotEmpty(row[7])) {
          operateDays.add(Days.WEDNESDAY.getDay());

        }
        if (StringUtils.isNotEmpty(row[8])) {
          operateDays.add(Days.THURSDAY.getDay());

        }
        if (StringUtils.isNotEmpty(row[9])) {
          operateDays.add(Days.FRIDAY.getDay());

        }
        if (StringUtils.isNotEmpty(row[10])) {
          operateDays.add(Days.SATUARDAY.getDay());

        }

      }
      flight.setServiceDays(operateDays);
      flights.add(flight);
    }
    return flights;
  }

}
