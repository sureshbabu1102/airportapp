package com.virgin.airport.app.airportapp.model;

public enum Days {
  SUNDAY("Sunday",1),MONDAY("Monday",2), TUESDAY("Tuesday",3),WEDNESDAY("Wednesday",4), THURSDAY("Thursday",5), FRIDAY("Friday",6), SATUARDAY("Saturday",7);
    private String day;

  private int value;
  Days(String day,int value){
    this.day =day;
    this.value = value;
  }

  public String getDay() {
    return day;
  }

  public void setDay(String day) {
    this.day = day;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }





}
